package springcore.beans.beans2;

import org.springframework.stereotype.Component;

@Component
public class NarcissusFlower {
  private String color;
  private int higth;
  private  boolean hasAroma;

  public NarcissusFlower() {
  }

  public NarcissusFlower(String color, int higth, boolean hasAroma) {
    this.color = color;
    this.higth = higth;
    this.hasAroma = hasAroma;
  }

  @Override
  public String toString() {
    return "NarcissusFlower{" +
        "color='" + color + '\'' +
        ", higth=" + higth +
        ", hasAroma=" + hasAroma +
        '}';
  }
}
