package springcore.beans.beans2;

import org.springframework.stereotype.Component;

@Component
//@Profile("componentCreating")
public class RoseFlower {

  private String color;
  private int high;
  private boolean hasAroma;
  private boolean hasSpines;

  public RoseFlower() {
  }

  public RoseFlower(String color, int high, boolean hasAroma, boolean hasSpines) {
    this.color = color;
    this.high = high;
    this.hasAroma = hasAroma;
    this.hasSpines = hasSpines;
  }

  @Override
  public String toString() {
    return "RoseFlower{" +
        "color='" + color + '\'' +
        ", higth=" + high +
        ", hasAroma=" + hasAroma +
        ", hasSpines=" + hasSpines +
        '}';
  }
}
