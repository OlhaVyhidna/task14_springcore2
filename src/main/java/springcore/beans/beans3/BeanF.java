package springcore.beans.beans3;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("thirdComponentProfile")
public class BeanF implements ProfilingInterface{
  private String name;
  private int value;
  private String category;

  @Override
  public String toString() {
    return "BeanF{" +
        "name='" + name + '\'' +
        ", value=" + value +
        ", category='" + category + '\'' +
        '}';
  }
}
