package springcore.beans.beans3;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("secondComponentProfile")
public class BeanE implements ProfilingInterface{
  private String name;
  private int value;
  private String category;

  @Override
  public String toString() {
    return "BeanE{" +
        "name='" + name + '\'' +
        ", value=" + value +
        ", category='" + category + '\'' +
        '}';
  }
}
