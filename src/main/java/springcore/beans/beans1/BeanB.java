package springcore.beans.beans1;

import org.springframework.stereotype.Component;

@Component
public class BeanB {
  private String name;
  private int value;
  private String category;

  @Override
  public String toString() {
    return "BeanB{" +
        "name='" + name + '\'' +
        ", value=" + value +
        ", category='" + category + '\'' +
        '}';
  }
}
