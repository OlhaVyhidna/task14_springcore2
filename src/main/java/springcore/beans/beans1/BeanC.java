package springcore.beans.beans1;

import org.springframework.stereotype.Component;

@Component
public class BeanC {
  private String name;
  private int value;
  private String category;

  @Override
  public String toString() {
    return "BeanC{" +
        "name='" + name + '\'' +
        ", value=" + value +
        ", category='" + category + '\'' +
        '}';
  }
}
