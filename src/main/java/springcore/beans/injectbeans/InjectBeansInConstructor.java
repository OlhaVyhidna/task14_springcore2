package springcore.beans.injectbeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InjectBeansInConstructor {

  private OtherBeanA otherBeanA;
  private OtherBeanB otherBeanB;
  private OtherBeanC otherBeanC;

  @Autowired
  public InjectBeansInConstructor(OtherBeanA otherBeanA,
      OtherBeanB otherBeanB, OtherBeanC otherBeanC) {
    this.otherBeanA = otherBeanA;
    this.otherBeanB = otherBeanB;
    this.otherBeanC = otherBeanC;
  }

  @Override
  public String toString() {
    return "InjectBeansInConstructor{" +
        "otherBeanA=" + otherBeanA +
        ", otherBeanB=" + otherBeanB +
        ", otherBeanC=" + otherBeanC +
        '}';
  }
}
