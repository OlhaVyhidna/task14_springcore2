package springcore.beans.injectbeans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Qualifier(value = "otherBeanCQualifier")
@Scope("prototype")
public class OtherBeanC {
  private String name;
  private int id;
}
