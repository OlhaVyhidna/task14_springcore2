package springcore.beans.injectbeans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Qualifier(value = "beanB2")
@Scope("singleton")
public class OtherBeanB {
  private String name;
  private int id;
}
