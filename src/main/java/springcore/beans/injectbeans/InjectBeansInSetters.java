package springcore.beans.injectbeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class InjectBeansInSetters {
  @Qualifier(value = "otherBeanA")
  private OtherBeanA otherBeanASetter;
  private OtherBeanB otherBeanBSetter;
  @Qualifier(value = "otherBeanCQualifier")
  private OtherBeanC otherBeanCSetter;

  @Autowired
  public void setOtherBeanASetter(OtherBeanA otherBeanASetter) {
    this.otherBeanASetter = otherBeanASetter;
  }

  @Autowired
  public void setOtherBeanBSetter(OtherBeanB otherBeanBSetter) {
    this.otherBeanBSetter = otherBeanBSetter;
  }

  @Autowired
  public void setOtherBeanCSetter(OtherBeanC otherBeanCSetter) {
    this.otherBeanCSetter = otherBeanCSetter;
  }

  @Override
  public String toString() {
    return "InjectBeansInSetters{" +
        "otherBeanASetter=" + otherBeanASetter +
        ", otherBeanBSetter=" + otherBeanBSetter +
        ", otherBeanCSetter=" + otherBeanCSetter +
        '}';
  }
}
