package springcore.beans.injectbeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class InjectBeansInFields {

  @Autowired
  @Qualifier(value = "otherBeanA")
  private OtherBeanA myOtherBeanA;
  @Autowired
  @Qualifier(value = "beanB2")
  private OtherBeanB myOtherBeanB;
  @Autowired
  private OtherBeanC myOtherBeanC;

  @Override
  public String toString() {
    return "InjectBeansInFields{" +
        "myOtherBeanA=" + myOtherBeanA +
        ", myOtherBeanB=" + myOtherBeanB +
        ", myOtherBeanC=" + myOtherBeanC +
        '}';
  }
}
