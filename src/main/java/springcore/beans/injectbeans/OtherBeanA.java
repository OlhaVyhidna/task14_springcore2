package springcore.beans.injectbeans;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class OtherBeanA {

  private String name;
  private int id;
}
