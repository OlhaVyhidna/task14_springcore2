package springcore.beans.order;

import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
@Primary
public class Lavender implements Flower {

  @Override
  public String getFlowerName() {
    return "Lavender";
  }
}
