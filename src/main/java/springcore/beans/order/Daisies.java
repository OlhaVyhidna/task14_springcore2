package springcore.beans.order;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(4)
public class Daisies implements Flower {

  @Override
  public String getFlowerName() {
    return "Daisies";
  }
}
