package springcore.beans.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class PrimaryAndQualifier {

  @Autowired
  private Flower primaryRealisation;
  @Autowired
  @Qualifier("roses")
  private Flower rosesFlowers;
  @Autowired
  @Qualifier("tulips")
  private Flower tulipsFlowers;
  @Autowired
  @Qualifier("daisies")
  private Flower daisiesFlowers;

  @Override
  public String toString() {
    return "PrimaryAndQualifier{" +
        "primaryRealisation=" + primaryRealisation +
        ", rosesFlowers=" + rosesFlowers +
        ", tulipsFlowers=" + tulipsFlowers +
        ", daisiesFlowers=" + daisiesFlowers +
        '}';
  }
}
