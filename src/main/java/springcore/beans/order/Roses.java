package springcore.beans.order;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(3)
public class Roses implements Flower {

  @Override
  public String getFlowerName() {
    return "Roses";
  }
}
