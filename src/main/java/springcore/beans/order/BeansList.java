package springcore.beans.order;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeansList {

  @Autowired
  List<Flower> flowers;

//  public void  printFlowers(){
//    for (Flower flower : flowers) {
//      System.out.println(flower.getFlowerName());
//    }
//  }


  public List<Flower> getFlowers() {
    return flowers;
  }
}
