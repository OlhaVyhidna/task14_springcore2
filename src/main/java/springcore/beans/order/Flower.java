package springcore.beans.order;

public interface Flower {
  String getFlowerName();

}
