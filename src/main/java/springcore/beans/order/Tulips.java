package springcore.beans.order;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class Tulips implements Flower {

  @Override
  public String getFlowerName() {
    return "Tulips";
  }
}
