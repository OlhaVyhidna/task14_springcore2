package springcore;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import springcore.configurations.FirstConfiguration;
import springcore.configurations.SecondConfiguration;

public class ApplicationContext {

  public static void main(String[] args) {
    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
        FirstConfiguration.class, SecondConfiguration.class
    );

    //profile example
//    context.getEnvironment().setActiveProfiles("secondBeanProfile");
//    context.registerBean(ProfileConfiguration.class);
//    context.refresh();
//    System.out.println(context.getBean(ProfilingInterface.class));

    String[] beanDefinitionNames = context.getBeanDefinitionNames();
    for (String beanDefinitionName : beanDefinitionNames) {
      System.out.println(beanDefinitionName);
    }
  }
}
