package springcore.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springcore.beans.beans3.BeanD;
import springcore.beans.beans3.BeanE;

@Configuration
public class ProfileConfiguration {

  @Bean
  @Profile("firstBeanProfile")
  public BeanD getBeanD(){
    return new BeanD();
  }

  @Bean
  @Profile("secondBeanProfile")
  public BeanE getBeanE(){
    return new BeanE();
  }
}
