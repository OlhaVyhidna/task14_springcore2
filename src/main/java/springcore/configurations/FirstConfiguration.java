package springcore.configurations;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(value = {"springcore.beans.beans1", "springcore.beans.injectbeans",
    "springcore.beans.order"})
public class FirstConfiguration {

}
