package springcore.configurations;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import springcore.beans.beans3.BeanE;

@Configuration
@ComponentScans({
    @ComponentScan(basePackages = "springcore.beans.beans2", useDefaultFilters = false,
        includeFilters = @Filter(type = FilterType.REGEX, pattern = {".*Flower$"})
    ),
    @ComponentScan(basePackages = "springcore.beans.beans3",
        excludeFilters = @Filter(type = FilterType.ASSIGNABLE_TYPE, classes = BeanE.class)
    )})
public class SecondConfiguration {

}
